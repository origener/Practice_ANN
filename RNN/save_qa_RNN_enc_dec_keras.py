import numpy as np
from keras import Sequential
from keras.callbacks import ModelCheckpoint
from keras.engine.saving import load_model
from keras.layers import LSTM, RepeatVector, TimeDistributed, Dense, Activation
from keras.optimizers import Adam
from sklearn.cross_validation import train_test_split
import matplotlib as plt
import os

MODEL_DIR = os.path.join(os.path.dirname(__file__), 'model')
if os.path.exists(MODEL_DIR) is False:
    os.mkdir(MODEL_DIR)

def n(digits=3):
    number = ''
    for i in range(np.random.randint(1, digits + 1)):
        number += np.random.choice(list('0123456789'))
    return int(number)

def padding(chars, maxlen):
    return chars + ' ' * (maxlen - len(chars))

digits = 3 # 각 숫자 최대 자릿수
input_digits = digits * 2 + 1 # 연산자 포함 인풋 크기
output_digits = digits + 1
N = 20000
N_train = 18000

added = set() # 집합함수 순서가 없고 중복을 허용하지 않음
questions = []
answers = []

while len(questions) < N:
    a, b, = n(), n()

    pair = tuple(sorted((a, b)))
    if pair in added:
        continue

    question = '{}+{}'.format(a, b)
    question = padding(question, input_digits)
    answer = str(a + b)
    answer = padding(answer, output_digits)

    added.add(pair)
    questions.append(question)
    answers.append(answer)

chars = '0123456789+ '
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))


X = np.zeros((len(questions), input_digits, len(chars)), dtype=np.integer)
Y = np.zeros((len(questions), output_digits, len(chars)), dtype=np.integer)

#one-hot
for i in range(N):
    for t, char in enumerate(questions[i]):
        X[i, t, char_indices[char]] = 1
    for t, char in enumerate(answers[i]):
        Y[i, t, char_indices[char]] = 1

X_train, X_validation, Y_train, Y_validation = \
    train_test_split(X, Y, train_size=N_train)

n_in = len(chars)
n_hidden = 128
n_out = len(chars)

#model = Sequential()
model = load_model(MODEL_DIR + '/model_cont.hdf5')
batch_size = 200
epochs = 300
'''
# 인코더
model.add(LSTM(n_hidden, input_shape=(input_digits, n_in)))

# 디코더
model.add(RepeatVector(output_digits))
model.add(LSTM(n_hidden, return_sequences=True))
model.add(TimeDistributed(Dense(n_out)))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer=Adam(lr=0.001, beta_1=0.9, beta_2=0.999),
              metrics=['accuracy'])
'''
checkpoint = ModelCheckpoint(
    filepath = os.path.join(MODEL_DIR, 'model_cont.hdf5'),
    save_best_only=True)

for epoch in range(epochs):
    hist = model.fit(X_train, Y_train, batch_size=batch_size, epochs=1,
                     validation_data=(X_validation, Y_validation),
                     callbacks=[checkpoint])

     #무작위 검증
    for i in range(1):
        index = np.random.randint(0, len(X_validation))
        question = X_validation[np.array([index])]
        answer = Y_validation[np.array([index])]
        prediction = model.predict_classes(question, verbose=0)

        question = question.argmax(axis=-1)
        answer = answer.argmax(axis=-1)

        q = ''.join(indices_char[i] for i in question[0])
        a = ''.join(indices_char[i] for i in answer[0])
        p = ''.join(indices_char[i] for i in prediction[0])

        print('-' * 10)
        print('Q:', q)
        print('A:', a)
        print('T/F:', end=' ')
        if a == p:
            print('T')
        else:
            print('F')

plt.rc('font', family='serif')
acc_plt = plt.figure().add_subplot(111)
acc_plt.plot(hist['val_loss'], color='red')
loss_plt = acc_plt.twinx()
loss_plt.plot(hist['val_acc'], color='black')
plt.show()

