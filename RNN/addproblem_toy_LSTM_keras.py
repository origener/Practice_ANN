import numpy as np
from keras import Sequential
from keras.callbacks import EarlyStopping
from keras.initializers import TruncatedNormal
from keras.layers import Dense, Activation, LSTM
from keras.optimizers import Adam
from sklearn.cross_validation import train_test_split
from keras.layers.recurrent import SimpleRNN
import matplotlib.pyplot as plt

def mask(T=200):
    mask = np.zeros(T)
    indices = np.random.permutation(np.arange(T))[:2]
    mask[indices] = 1
    return mask

def mask_toy_problem(N=10, T=200):
    signals = np.random.uniform(low=0.0, high=1.0, size=(N, T))
    masks = np.zeros((N, T))
    for i in range(N):
        masks[i] = mask(T)

    data = np.zeros((N, T, 2))
    data[:, :, 0] = signals[:]
    data[:, :, 1] = masks[:]
    target = (signals * masks).sum(axis=1).reshape(N, 1)

    return data, target

def sin(x, T=100):
    return np.sin(2.0 * np.pi * x / T)

def sign_toy_problem(T =100, ampl=0.05):
    x = np.arange(0, 2 * T + 1)
    noise = ampl * np.random.uniform(low=-1.0, high=1.0, size=len(x))
    return sin(x) + noise

N = 10000
T = 200
maxlen = T

X, Y = mask_toy_problem(N=N, T=T)

N_train = int(N * 0.9)
N_validation = N - N_train

X_train, X_validation, Y_train, Y_validation \
    = train_test_split(X, Y, test_size=N_validation)


n_in = len(X[0][0]) # 1
n_hidden = 20
n_out = len(Y[0]) # 1

model = Sequential()
'''
model.add(SimpleRNN(n_hidden,
                    kernel_initializer=TruncatedNormal(stddev=0.01),
                    input_shape=(maxlen, n_out)))
'''
model.add(LSTM(n_hidden,
                    kernel_initializer=TruncatedNormal(stddev=0.01),
                    input_shape=(maxlen, 2)))
model.add(Dense(n_out, kernel_initializer=TruncatedNormal(stddev=0.01)))
model.add(Activation('linear'))

optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999)
model.compile(loss='mean_squared_error', optimizer=optimizer)

epochs = 500
batch_size = 10
early_stopping = EarlyStopping(patience=10, verbose=1)
hist = model.fit(X_train, Y_train,
          batch_size=batch_size,
          epochs=epochs,
          validation_data=(X_validation, Y_validation),
          callbacks=[early_stopping])

val_loss = hist.history['val_loss']

plt.rc('font', family='serif')
plt.figure()
plt.plot(val_loss, color='black')

plt.show()