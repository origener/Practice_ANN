import numpy as np
from sklearn import datasets
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
from tensorflow.contrib import rnn

''' "정석으로 배우는 딥러닝"의 심층신경망 부분의 내용을 종합한 소스코드
   - mnist 데이터 사용
   - tensorflow class 화
   - train / validation / test 데이터 분류
   - 각 epoch마다 validation 데이터로 검증 후 출력 마지막에 테스트 데이터로 검증 
   - batch normalization 사용
   - dropout 구현 (batch_normalization으로 은닉층에서는 사용하지 않음
   - weight 초기화 truncated_normal 사용
   - matplotlib를 사용한 시각화 
   - 활성화 함수는 relu 사용 
   - 학습률 최적화를 RMSprop 사용 // 각 optimizer 별 구현은 optimizer_comp_tf_keras.py에
   - 입력 데이터의 정규화
'''
np.random.seed(0)
tf.set_random_seed(1234)

'''EarlyStopping 클래스 - 오차가 늘어난 시점에서 patience 만큼 에폭이 지나도 step이 시작된 
   시점의 오차 이하로 낮아지지 않으면 early stopping 되는 함수 '''

class EarlyStopping():
    def __init__(self, patience=0, verbose=0):
        self._step = 0
        self._loss = float('inf')  # 무한
        self.patience = patience
        self.verbose = verbose

    def validate(self, loss):
        if self._loss < loss:
            self._step += 1
            print('earlystop step : ', self._step)
            if self._step > self.patience:
                if self.verbose:
                    print('early stopping')
                return True
        else:
            self._step = 0
            self._loss = loss
        return False

'''DNN 클래스 - tensorflow 를 위한 inferrence, loss, training 등 기본함수들 제공'''
class RNN(object):
    def __init__(self, n_in, n_time, n_hiddens, n_out):
        # 초기화 처리
        self.n_in = n_in
        self.n_time = n_time
        self.n_hiddens = n_hiddens
        self.n_out = n_out
        self.weights = []
        self.biases = []
        self._x = None
        self._y = None
        self._t = None
        self._keep_prob = None
        self._sess = None
        self._history = {
            'val_acc': [],
            'val_loss': []
        }

    # 배치 정규화 함수 tensorflow에서는 tf.nn.batch_normalization 라는 api 제공함
    def batch_normalization(self, shape, x):
        eps = 1e-8 #앱실론 아주 작은 수를 더해 분모가 0이 되지 않게 한다.
        beta = tf.Variable(tf.zeros(shape)) # 배치 정규화 시 필요한 새로운 매개변수
        gamma = tf.Variable(tf.ones(shape)) # bias를 대체하는 새로운 매개변수
        mean, var = tf.nn.moments(x, [0]) # 분산과 평균으로 입력 각 배치별 x를 정규화 시킨다.
        return gamma * (x - mean) / tf.sqrt(var + eps) + beta

    # weight 초기화를 위해 truncated_normal로 필요시 W를 초기화
    def weight_variable(self, shape):
        initial = tf.truncated_normal(shape, stddev=0.01)
        return tf.Variable(initial)

    # Bias 초기화
    def bias_variable(self, shape):
        initial = tf.zeros(shape)
        return tf.Variable(initial)

    # 모델을 정의하고 주어진 hiddens 배열에 따라 은닉 층 및 출력층 생성
    # 각 층의 batch_normalization 수행
    # 각 층의 W 와 B 생성 및 self 에 저장 마지막 출력 y 리턴
    def inference(self, x, keep_prob):
        # 모델 정의

        '''DNN
        # 입력층 - 은닉층
        for i, n_hidden in enumerate(self.n_hiddens):
            if i == 0:
                input = x
                input_dim = self.n_in
            else:
                input = output
                input_dim = self.n_hiddens[i - 1]

            W = self.weights.append(self.weight_variable([input_dim, n_hidden]))

            # 배치표준화 하지않고 dropout 사용
            # b = self.biases.append(self.bias_variable([n_hidden]))
            # h = tf.nn.relu(tf.matmul(input, self.weights[-1]) + self.biases[-1])
            # output = tf.nn.dropout(h, keep_prob)

            # 패치 표준화 사용 (bias가 필요없음)
            u = tf.matmul(input, self.weights[-1])
            h = self.batch_normalization([n_hidden], u)
            output = tf.nn.relu(h)

        # 은닉층 - 출력층
        self.weights.append(self.weight_variable([self.n_hiddens[-1], self.n_out]))
        self.biases.append(self.bias_variable([self.n_out]))
        y = tf.nn.softmax(tf.matmul(output, self.weights[-1]) + self.biases[-1])
        '''

        # BiRNN (static_bidirectional_rnn API가 sequence 입력만 받도록 바뀌어서 재작업
        x = tf.transpose(x, [1, 0, 2]) # 텐서의 None 차원을 가운데로 옮기고
        x = tf.reshape(x, [-1, n_in]) # [None , [28 , 28]] 으로 reshape
        x = tf.split(x, n_time, 0) # None개의 28개 의 배열로 분리
        # 결국 위의 작업은 [28, 28] 의 None개 차원의 텐서를 None차원을 유지하면서 28개씩 sequence로 바꾼다

        cell_forward = rnn.BasicLSTMCell(self.n_hiddens, forget_bias=1.0)
        cell_backward = rnn.BasicLSTMCell(self.n_hiddens, forget_bias=1.0)
        output, _, _ = rnn.static_bidirectional_rnn(cell_forward, cell_backward, x, dtype=tf.float32)

        W = self.weight_variable([self.n_hiddens * 2, self.n_out])
        b = self.bias_variable([self.n_out])
        y = tf.nn.softmax(tf.matmul(output[-1], W) + b)

        return y

    # cross_entropy를 정의하고 리턴 // 출력 y 의 상한을 제한하는 clip_by_value처리
    def loss(self, y, t):
        # 오차함수 정의

        cross_entropy = tf.reduce_mean(-tf.reduce_sum(
            t * tf.log(tf.clip_by_value(y, 1e-10, 1.0)),
            axis=1))

        # cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=y, labels=t)
        return cross_entropy

    # optimizer 로 RMSProp 사용
    def training(self, loss):
        # 학습 알고리즘 정의
        optimizer = tf.train.RMSPropOptimizer(0.001)
        train_step = optimizer.minimize(loss)
        return train_step

    # 평가를 위한 accuracy를 설정하고 평균을 리턴
    def accuracy(self, y, t):
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(t, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        return accuracy

    def fit(self, X_train, Y_train, epochs=100, batch_size=100, p_keep=0.5, verbose=1):
        # 학습 처리
        #x = tf.placeholder(tf.float32, shape=[None, self.n_in])
        x = tf.placeholder(tf.float32, shape=[None, self.n_time, self.n_in])
        t = tf.placeholder(tf.float32, shape=[None, self.n_out])
        keep_prob = tf.placeholder(tf.float32)

        # evaluate() 용
        self._x = x
        self._t = t
        self._keep_prob = keep_prob

        y = self.inference(x, keep_prob)
        loss = self.loss(y, t)
        train_step = self.training(loss)
        accuracy = self.accuracy(y, t)

        init = tf.global_variables_initializer()
        sess = tf.Session()
        sess.run(init)

        # evaluate() 용
        self._y = y
        self._sess = sess

        N_train = len(X_train)
        n_batches = N_train // batch_size

        # early stopping 용
        early_stopping = EarlyStopping(patience=20, verbose=1)

        for epoch in range(epochs):
            X_, Y_ = shuffle(X_train, Y_train)

            for i in range(n_batches):
                start = i * batch_size
                end = start + batch_size

                sess.run(train_step, feed_dict={
                    x: X_[start:end],
                    t: Y_[start:end],
                    keep_prob: p_keep
                })
            val_loss = loss.eval(session=sess, feed_dict={
                x: X_validation,
                t: Y_validation,
                keep_prob: 1.0
            })
            val_accuracy = accuracy.eval(session=sess, feed_dict={
                x: X_validation,
                t: Y_validation,
                keep_prob: 1.0
            })

            # 값 기록
            self._history['val_loss'].append(val_loss)
            self._history['val_acc'].append(val_accuracy)

            if verbose:
                print('epoch: ', epoch,
                      'validation loss: ', val_loss,
                      'validation accuracy: ', val_accuracy)

            #EarlyStopping
            if early_stopping.validate(val_loss):
                break

        return self._history
    # 학습이 끝나고 테스트 데이터를 이용하여 평가
    def evaluate(self, X_test, Y_test):
        # 평가 처리
        accuracy = self.accuracy(self._y, self._t)
        return accuracy.eval(session=self._sess, feed_dict={
            self._x: X_test,
            self._t: Y_test,
            self._keep_prob: 1.0
        })


if __name__ == '__main__':
    # 1. 데이터 준비
    mnist = datasets.fetch_mldata('MNIST original', data_home='.')

    epochs = 50
    n = len(mnist.data)
    N = 30000
    N_train = 20000
    N_validation = 4000
    indices = np.random.permutation(range(n))[:N]

    '''입력 데이터 정규화'''
    X = mnist.data[indices]
    X = X / 255.0
    X = X - X.mean(axis=1).reshape(len(X), 1)
    X = X.reshape(len(X), 28, 28)

    y = mnist.target[indices]
    Y = np.eye(10)[y.astype(int)]

    X_train, X_test, Y_train, Y_test = \
        train_test_split(X, Y, train_size=0.8)

    X_train, X_validation, Y_train, Y_validation = \
        train_test_split(X_train, Y_train, test_size=N_validation)

    n_in = 28
    n_time = 28
    n_hiddens = 128
    n_out = 10

    model = RNN(n_in=n_in, n_time=n_time, n_hiddens=n_hiddens, n_out=n_out)
    model.fit(X_train, Y_train, epochs=epochs, batch_size=200, p_keep=0.5)
    accuracy = model.evaluate(X_test, Y_test)
    print('accuracy: ', accuracy)

    plt.rc('font', family='serif')
    fig = plt.figure()

    ax_acc = fig.add_subplot(111)
    ax_acc.plot(range(len(model._history['val_acc'])), model._history['val_acc'],
                label='acc', color='black')
    ax_loss = ax_acc.twinx()
    ax_loss.plot(range(len(model._history['val_acc'])), model._history['val_loss'],
                 label='loss', color='gray')
    plt.xlabel('epochs')
    plt.legend(loc=4)  # 범례
    plt.grid(True)  # 격자
    '''
    plt.plot(range(epoch), model._history['val_acc'], label='acc', color='black')

    plt.xlabel('epoch')
    plt.ylabel('validation accuracy')
    '''

    plt.show()

