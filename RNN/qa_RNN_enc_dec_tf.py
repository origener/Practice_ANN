import numpy as np
import tensorflow as tf
from sklearn.cross_validation import train_test_split
from sklearn.utils import shuffle
from tensorflow.contrib import rnn
import matplotlib.pyplot as plt

# weight 초기화를 위해 truncated_normal로 필요시 W를 초기화
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.01)
    return tf.Variable(initial)

# Bias 초기화
def bias_variable(shape):
    initial = tf.zeros(shape)
    return tf.Variable(initial)

def inference(x, y, n_batch, is_training, input_digits=None, output_digits=None, n_hidden=None, n_out=None):
    # 인코더
    encoder = rnn.BasicLSTMCell(n_hidden, forget_bias=1.0)
    state = encoder.zero_state(n_batch, tf.float32)
    encoders_outputs = []
    encoders_states = []

    with tf.variable_scope('Encoder'):
        for t in range(input_digits):
            if t > 0:
                tf.get_variable_scope().reuse_variables()
            (output, state) = encoder(x[:, t, :], state)
            encoders_outputs.append(output)
            encoders_states.append(state)

    # 디코더
    decoder = rnn.BasicLSTMCell(n_hidden, forget_bias=1.0)
    state = encoders_states[-1]
    decoder_outputs = [encoders_outputs[-1]]

    V = weight_variable([n_hidden, n_out])
    c = bias_variable([n_out])
    outputs = []

    with tf.variable_scope('Decoder'):
        for t in range(1, output_digits):
            if t > 1:
                tf.get_variable_scope().reuse_variables()
            if is_training is True:
                (output, state) = decoder(y[:, t - 1, :], state)
            else:
                #학습이 아닐때는 직전의 출력을 입력에 사용
                linear = tf.matmul(decoder_outputs[-1], V) + c
                out = tf.nn.softmax(linear)
                outputs.append(out)
                out = tf.one_hot(tf.argmax(out, -1), depth=output_digits)
                (output, state) = decoder(out, state)

            decoder_outputs.append(output)


    # 최종 출력
    if is_training is True:
        output = tf.reshape(tf.concat(decoder_outputs, axis=1),
                            [-1, output_digits, n_hidden])
        linear = tf.einsum('ijk,kl->ijl', output, V) +  c
        return tf.nn.softmax(linear)
    else:
        linear = tf.matmul(decoder_outputs[-1], V) + c
        out = tf.nn.softmax(linear)
        outputs.append(out)
        output = tf.reshape(tf.concat(outputs, axis=1),
                            [-1, output_digits, n_out])

        return output
def loss(y, t):
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(
        t * tf.log(tf.clip_by_value(y, 1e-10, 1.0)), reduction_indices=[1]))
    return cross_entropy

def training(loss):
    optimizer = tf.train.AdamOptimizer(learning_rate=0.001, beta1=0.9, beta2=0.999)
    train_step = optimizer.minimize(loss)
    return train_step

def accuracy(y, t):
    correct_prediction = tf.equal(tf.argmax(y, -1), tf.argmax(t, -1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    return accuracy

def n(digits=3):
    number = ''
    for i in range(np.random.randint(1, digits + 1)):
        number += np.random.choice(list('0123456789'))
    return int(number)

def padding(chars, maxlen):
    return chars + ' ' * (maxlen - len(chars))

digits = 3 # 각 숫자 최대 자릿수
input_digits = digits * 2 + 1 # 연산자 포함 인풋 크기
output_digits = digits + 1
N = 20000
N_train = 16000

added = set() # 집합함수 순서가 없고 중복을 허용하지 않음
questions = []
answers = []

while len(questions) < N:
    a, b, = n(), n()

    pair = tuple(sorted((a, b)))
    if pair in added:
        continue

    question = '{}+{}'.format(a, b)
    question = padding(question, input_digits)
    answer = str(a + b)
    answer = padding(answer, output_digits)

    added.add(pair)
    questions.append(question)
    answers.append(answer)

chars = '0123456789+ '
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

X = np.zeros((len(questions), input_digits, len(chars)), dtype=np.integer)
Y = np.zeros((len(questions), output_digits, len(chars)), dtype=np.integer)

#one-hot
for i in range(N):
    for t, char in enumerate(questions[i]):
        X[i, t, char_indices[char]] = 1
    for t, char in enumerate(answers[i]):
        Y[i, t, char_indices[char]] = 1

X_train, X_validation, Y_train, Y_validation = \
    train_test_split(X, Y, train_size=N_train)

n_in = len(chars)
n_hidden = 128
n_out = len(chars)
batch_size = 200
n_batches = N_train // batch_size
N_validation = N - N_train
epochs = 100

x = tf.placeholder(tf.float32, shape=[None, input_digits, n_in])
t = tf.placeholder(tf.float32, shape=[None, output_digits, n_out])
n_batch = tf.placeholder(tf.int32, [], name='n_batch')
is_training = tf.placeholder(tf.bool)

y = inference(x, t, n_batch, is_training, input_digits=input_digits,
              output_digits=output_digits, n_hidden=n_hidden, n_out=n_out)
loss = loss(y, t)
train_step = training(loss)
acc = accuracy(y, t)

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)
history = {
    'val_acc': [],
    'val_loss': []
}
for epoch in range(epochs):
    X_, Y_ = shuffle(X_train, Y_train)

    for i in range(n_batches):
        start = i * batch_size
        end = start + batch_size

        a = sess.run(train_step, feed_dict={
            x: X_[start:end],
            t: Y_[start:end],
            n_batch: batch_size,
            is_training: True
        })

    val_loss = loss.eval(session=sess, feed_dict={
        x: X_validation,
        t: Y_validation,
        n_batch: N_validation,
        is_training: False
    })

    val_acc = acc.eval(session=sess, feed_dict={
        x: X_validation,
        t: Y_validation,
        n_batch: N_validation,
        is_training: False
    })

    history['val_loss'].append(val_loss)
    history['val_acc'].append(val_acc)

    #무작위 검증
    for i in range(1):
        index = np.random.randint(0, N_validation)
        question = X_validation[np.array([index])]
        answer = Y_validation[np.array([index])]
        prediction = y.eval(session=sess, feed_dict={
            x: question,
            n_batch: 1,
            is_training: False
        })

        question = question.argmax(axis=-1)
        answer = answer.argmax(axis=-1)
        prediction = np.argmax(prediction, -1)

        q = ''.join(indices_char[i] for i in question[0])
        a = ''.join(indices_char[i] for i in answer[0])
        p = ''.join(indices_char[i] for i in prediction[0])

        print('-' * 10)
        print('epoch: ', epoch,
              'validation loss: ', val_loss,
              'validation accuracy: ', val_acc)
        print('Q:', q)
        print('A:', a)
        print('T/F:', end=' ')
        if a == p:
            print('T')
        else:
            print('F')

plt.rc('font', family='serif')
acc_plt = plt.figure().add_subplot(111)
acc_plt.plot(history['val_loss'], color='red')
loss_plt = acc_plt.twinx()
loss_plt.plot(history['val_acc'], color='black')
plt.show()


