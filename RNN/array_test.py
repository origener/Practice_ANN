import numpy as np
import tensorflow as tf

x = np.arange(5 * 5)
x = x.reshape(1, 5, 5)
print(x)
x = np.transpose(x, [1, 0, 2])
print(x)
# x : 28, none, 28
x = np.reshape(x, [-1, 5])
print(x)
# x : none, 28
x = np.split(x, 5, 0)
print(x)