import numpy as np
from sklearn.cross_validation import train_test_split


def sin(x, T=100):
    return np.sin(2.0 * np.pi * x / T)

def toy_problem(T =100, ampl=0.05):
    x = np.arange(0, 2 * T + 1)
    noise = ampl * np.random.uniform(low=-1.0, high=1.0, size=len(x))
    return sin(x) + noise

T = 100
f = toy_problem(T)

length_of_sequences = 2 * T
maxlen = 25

data = []
target = []

for i in range(0, length_of_sequences - maxlen + 1):
    data.append(f[i: i + maxlen])
    target.append(f[i + maxlen])

X = np.array(data).reshape(len(data), maxlen, 1) # maxlen으로 나눈 전체 데이터셋을 각각 maxlen개의 원소로 나누어줌
Y = np.array(target).reshape(len(data), 1)

N_train = int(len(data) * 0.9)
N_validation = len(data) - N_train

X_train, X_validation, Y_train, Y_validation \
    = train_test_split(X, Y, test_size=N_validation)

