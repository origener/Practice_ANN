import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets('MNIST_data', one_hot=True, reshape=False)

X = tf.placeholder(tf.float32, shape=[None, 28, 28, 1])
Y = tf.placeholder(tf.float32, shape=[None, 10])



kernel_1 = tf.Variable(tf.truncated_normal(shape=[4,4,1,4], stddev=0.1))
bias_1 = tf.Variable(tf.truncated_normal(shape=[4], stddev=0.1))
convolution_1 = tf.nn.conv2d(X, kernel_1, strides=[1,1,1,1],
                             padding='SAME') + bias_1
activation_1 = tf.nn.relu(convolution_1)
maxpool_1 = tf.nn.max_pool(activation_1, ksize=[1,2,2,1],
                           strides=[1,2,2,1], padding='SAME')

kernel_2 = tf.Variable(tf.truncated_normal(shape=[2,2,4,8], stddev=0.1))
bias_2 = tf.Variable(tf.truncated_normal(shape=[8], stddev=0.1))
convolution_2 = tf.nn.conv2d(maxpool_1, kernel_2, strides=[1,1,1,1],
                             padding='SAME') + bias_2
activation_2 = tf.nn.relu(convolution_2)
maxpool_2 = tf.nn.max_pool(activation_2, ksize=[1,2,2,1],
                           strides=[1,2,2,1], padding='SAME')

w = tf.Variable(tf.truncated_normal(shape=[8*7*7, 10]))
b = tf.Variable(tf.truncated_normal(shape=[10]))
maxpool_2_flat = tf.reshape(maxpool_2, [-1, 8*7*7])
output = tf.matmul(maxpool_2_flat, w) + b

loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=output))
train_step = tf.train.AdamOptimizer(0.005).minimize(loss)

correct_prediction = tf.equal(tf.argmax(output, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

with tf.Session() as sess:
    print("Start....")
    sess.run(tf.global_variables_initializer())
    for i in range(10000):
        X_train, Y_train = mnist.train.next_batch(256)
        sess.run(train_step, feed_dict={
            X: X_train,
            Y: Y_train
        })
        if i%100 :
            print(sess.run(accuracy, feed_dict={
                X: mnist.test.images,
                Y: mnist.test.labels
            }))
