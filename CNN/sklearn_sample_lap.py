import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.datasets import load_sample_image


china = load_sample_image("china.jpg")
flower = load_sample_image("flower.jpg")


def inference(x, n):
    k = np.float32([1, 4, 6, 4, 1])
    k = np.outer(k, k)
    k5x5 = k[:, :, None, None] / k.sum() * np.eye(3, dtype=np.float32)

    def split(x):
        with tf.name_scope('split'):
            lo = tf.nn.conv2d(x, k5x5, [1, 2, 2, 1], 'SAME')
            lo2 = tf.nn.conv2d_transpose(lo, k5x5 * 4, tf.shape(x), [1, 2, 2, 1])
            hi = x - lo2
        return hi, lo

    def split_n(x, n):
        levels = []
        for i in range(n):
            hi, x = split(x)
            levels.append(hi)
        levels.append(x)
        return levels[::-1]

    def nomalize_std(x, eps=1e-10):
        with tf.name_scope('normalize'):
            std = tf.sqrt(tf.reduce_mean(tf.square(x)))
            return x / tf.maximum(std, eps)

    def lap_merge(levels):
        img = levels[0]
        for hi in levels[1:]:
            with tf.name_scope('merge'):
                img = tf.nn.conv2d_transpose(img, k5x5 * 4, tf.shape(hi), [1, 2, 2, 1]) + hi
        return img

    t_level = split_n(x, n)
    t_level = list(map(nomalize_std, t_level))
    out = lap_merge(t_level)
    return out[0,:,:,:]

def visstd(a, s=0.1):
    '''Normalize the image range for visualization'''
    return (a-a.mean())/max(a.std(), 1e-4)*s + 0.5

dataset = np.array([china, flower], dtype=np.float32)
height, width, channels = china.shape

X = tf.placeholder(tf.float32, shape=(None, height, width, channels))
lap = inference(X, n=1)
china = np.reshape(china, [1,height, width, channels])

with tf.Session() as sess:
    lap_out = sess.run(lap, feed_dict={X: china})
china = np.reshape(china, [height, width, channels])
china = china + lap_out
print(china)

filters = np.zeros(shape=(7, 7, channels, 2), dtype=np.float32)
filters[:, 3, :, 0] = 1
filters[3, :, :, 1] = 1

'''
X = tf.placeholder(tf.float32, shape=(None, height, width, channels))
convolution = tf.nn.conv2d(X, filters, strides=[1,2,2,1], padding="SAME")

with tf.Session() as sess:
    output = sess.run(convolution, feed_dict={X: dataset})

X = tf.placeholder(tf.float32, shape=(None, height, width, channels))
max_pool = tf.nn.max_pool(X, ksize=[1,2,2,1], strides=[1,2,2,1], padding="VALID")
with tf.Session() as sess:
    output = sess.run(max_pool, feed_dict={X: dataset})
#plt.imshow(output[0, :, :, 1], cmap="gray")
'''
plt.imshow(china)
'''
plt.subplot(221).imshow(china)
plt.subplot(222).imshow(output_lo[0, :, : ,1])

plt.subplot(223).imshow(hi[0, :, :, 1])
plt.subplot(224).imshow(output_hi_norm[0, :, :, 1])
'''
plt.show()

