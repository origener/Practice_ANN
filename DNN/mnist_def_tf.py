import numpy as np
from sklearn import datasets
import tensorflow as tf
from sklearn.model_selection import train_test_split

np.random.seed(0)
tf.set_random_seed(1234)

def inference(x, keep_prob, n_in, n_hiddens, n_out):
    #모델정의
    def weight_variable(shape):
        initial = tf.truncated_normal(shape, stddev=0.01)
        return tf.Variable(initial)

    def bias_variable(shape):
        initial = tf.zeros(shape)
        return tf.Variable(initial)

    #입력층 - 은닉층
    for i, n_hidden in enumerate(n_hiddens):
        if i == 0:
            input = x
            input_dim = n_in
        else:
            input = output
            input_dim = n_hiddens[i-1]

        W = weight_variable([input_dim, n_hidden])
        b = bias_variable([n_hidden])

        h = tf.nn.relu(tf.matmul(input, W) + b)
        output = tf.nn.dropout(h, keep_prob)

    #은닉층 - 출력층
    W_out = weight_variable([n_hiddens[-1], n_out])
    b_out = bias_variable([n_out])
    y = tf.nn.softmax(tf.matmul(output, W_out) + b_out)
    return y

def loss(y, t):
    #오차함수 정의

    cross_entropy = tf.reduce_mean(-tf.reduce_sum(
        t * tf.log(tf.clip_by_value(y, 1e-10, 1.0)),
        axis=1))

    #cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=y, labels=t)
    return cross_entropy

def training(loss):
    #학습 알고리즘 정의
    optimizer = tf.train.GradientDescentOptimizer(0.01)
    train_step = optimizer.minimize(loss)
    return train_step

def accuracy(y, t):
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(t, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    return accuracy


if __name__ == '__main__':
    # 1. 데이터 준비
    mnist = datasets.fetch_mldata('MNIST original', data_home='.')

    n = len(mnist.data)
    N = 30000
    N_train = 20000
    N_validation = 4000
    indices = np.random.permutation(range(n))[:N]

    X = mnist.data[indices]
    y = mnist.target[indices]
    Y = np.eye(10)[y.astype(int)]

    X_train, X_test, Y_train, Y_test = \
        train_test_split(X, Y, train_size=0.8)

    X_train, X_validation, Y_train, Y_validation = \
        train_test_split(X_train, Y_train, test_size=N_validation)

    '''
    노멀화 시키면 안된다.. 왜?? 
    
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    X_train /= 255
    X_test /= 255
    '''

    # 2. 모델 설정
    n_in = len(X[0])  # 784 (28 * 28)
    n_hiddens = [256, 256, 256]
    n_out = len(Y[0])  # 10
    p_keep = 0.5

    x = tf.placeholder(tf.float32, shape=[None, n_in])
    t = tf.placeholder(tf.float32, shape=[None, n_out])
    keep_prob = tf.placeholder(tf.float32)

    y = inference(x, keep_prob, n_in=n_in, n_hiddens=n_hiddens, n_out=n_out)

    loss = loss(y, t)
    train_step = training(loss)

    accuracy = accuracy(y, t)

    history = {
        'val_loss': [],
        'val_acc': []
    }

    #3. 모델 학습
    epochs = 20
    batch_size = 200  # 미니배치 크기
    n_batchs = len(X_train) // batch_size

    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)

    for epoch in range(epochs):

        for i in range(n_batchs):
            start = i * batch_size
            end = start + batch_size
            sess.run(train_step, feed_dict={
                x: X_train[start:end],
                t: Y_train[start:end],
                keep_prob: p_keep
            })
    #4. 모델 평가
    accuracy_rate = accuracy.eval(session=sess, feed_dict={
        x: X_test,
        t: Y_test,
        keep_prob: p_keep
    })
    print('accuracy: ', accuracy_rate)

