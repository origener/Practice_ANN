from keras.callbacks import EarlyStopping
from keras.initializers import he_normal, zeros
from sklearn import datasets
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, BatchNormalization
from keras.optimizers import SGD, Adam
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from keras import backend as K

'''
def weight_variable(shape):
    return K.truncated_normal(shape, stddev=0.01)
'''

np.random.seed(0)

mnist = datasets.fetch_mldata('MNIST original', data_home='.')

epochs = 50
'''
n = len(mnist.data)
N = 30000
N_train = 20000
N_validation = 4000
indices = np.random.permutation(range(n))[:N]
'''
#X = mnist.data[indices]
X = mnist.data
X = X / 255.0
X = X - X.mean(axis=1).reshape(len(X), 1)

#y = mnist.target[indices]
y = mnist.target
Y = np.eye(10)[y.astype(int)]

n_in = len(X[0])
n_hiddens = [200, 200, 200]
n_out = len(Y[0])
p_keep = 0.5
activation = 'relu'

X_train, X_test, Y_train, Y_test = \
    train_test_split(X, Y, train_size=60000)

X_train, X_validation, Y_train, Y_validation = \
    train_test_split(X_train, Y_train, test_size=10000)

model = Sequential()
for i, input_dim in enumerate(([n_in] + n_hiddens)[:-1]):
    model.add(Dense(n_hiddens[i], input_dim=input_dim,
                    kernel_initializer='he_normal',
                    bias_initializer='zeros'))
    model.add(BatchNormalization())
    model.add(Activation(activation))
    #model.add(Dropout(p_keep))

model.add(Dense(n_out, kernel_initializer='he_normal', bias_initializer='zeros'))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001, beta_1=0.9, beta_2=0.999),
              metrics=['accuracy'])

epochs = 50
batch_size = 200
early_stopping = EarlyStopping(monitor='val_loss', patience=10, verbose=1)
hist = model.fit(X_train, Y_train, epochs=epochs, batch_size=200,
                 validation_data=(X_validation, Y_validation),
                 callbacks=[early_stopping])


val_acc = hist.history['val_acc']
val_loss = hist.history['val_loss']

eval_result = model.evaluate(X_test, Y_test)
print('evaluate :', eval_result)

plt.rc('font', family='serif')
fig = plt.figure()

ax_val_acc = fig.add_subplot(111)
ax_val_acc.plot(range(len(val_acc)), val_acc,
            label='val_acc', color='black')
ax_val_loss = ax_val_acc.twinx()
ax_val_loss.plot(range(len(val_loss)), val_loss,
             label='val_loss', color='gray')

#plt.plot(range(epochs), val_acc, label='acc', color='black')
plt.xlabel('epoch')
plt.ylabel('accuracy')
plt.legend(loc=4) #범례
plt.grid(True) #격자
plt.show()

