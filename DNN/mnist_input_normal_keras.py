from keras.initializers import TruncatedNormal
from sklearn import datasets
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, LeakyReLU
from keras.optimizers import SGD, RMSprop
from sklearn.model_selection import train_test_split

mnist = datasets.fetch_mldata('MNIST original', data_home='.')

n = len(mnist.data)
N = 10000
indices = np.random.permutation(range(n))[:N]
X = mnist.data[indices]
y = mnist.target[indices]

Y = np.eye(10)[y.astype(int)]
X_train, X_test, Y_train, Y_test = \
    train_test_split(X, Y, train_size=0.8)
# 예제대로만 하면 relu를 썼을 때 학습에 실패한다
# grayscale을 nomalize를 하지않으면 왜 안될까???
# 아래는 keras 의 example mnist에서 nomalize하는 방식
# -> batch_size를 200으로 하니까 된다.. 뭐냐이거
# -> 또 안되는데 나중에 작가가 알려줬다 .. 매개변수 초기화 할때 정규분포로 하면 된다고
# 결론 ->  w의 초기화가 골고루 안되어 있는 상태에서 x 값이 normalized되지 않고 들어오면
# 값이 너무 커져서 loss가 0에 수렴 경사소실되었던듯 normalized하면 x가 작아져서 괜찮은것 처럼 작동하고
'''
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255
'''
#모델 설정

n_in = len(X[0]) #784 (28 * 28)
n_hidden = 200
n_out = len(Y[0]) # 10

model = Sequential()
model.add(Dense(n_hidden, input_dim=n_in, kernel_initializer=TruncatedNormal(stddev=0.01)))
model.add(Activation('relu'))

model.add(Dense(n_hidden, kernel_initializer=TruncatedNormal(stddev=0.01)))
model.add(Activation('relu'))

model.add(Dense(n_hidden, kernel_initializer=TruncatedNormal(stddev=0.01)))
model.add(Activation('relu'))

model.add(Dense(n_hidden, kernel_initializer=TruncatedNormal(stddev=0.01)))
model.add(Activation('relu'))

model.add(Dense(n_out))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer=SGD(lr=0.01),
              metrics=['accuracy'])

#모델 학습

epochs = 100
batch_size = 200

model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size)

#예측 평가

loss_and_metrics = model.evaluate(X_test, Y_test)
print(loss_and_metrics)