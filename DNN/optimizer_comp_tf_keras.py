import numpy as np
from keras import Sequential
from keras.initializers import TruncatedNormal
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import SGD, Adagrad, Adadelta, RMSprop, Adam
from sklearn import datasets
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
import matplotlib.pyplot as plt

np.random.seed(0)
tf.set_random_seed(1234)
class DNN(object):
    def __init__(self, n_in, n_hiddens, n_out):
        #초기화 처리
        self.n_in = n_in
        self.n_hiddens = n_hiddens
        self.n_out = n_out
        self.weights = []
        self.biases = []
        self._x = None
        self._y = None
        self._t = None
        self._keep_prob = None
        self._sess = None
        self._history = {
            'SGD_val_acc': [],
            'SGD_val_loss': [],
            'momentum_val_acc': [],
            'momentum_val_loss': [],
            'Nesterov_val_acc': [],
            'Nesterov_val_loss': [],
            'Adagrad_val_acc': [],
            'Adagrad_val_loss': [],
            'Adadelta_val_acc': [],
            'Adadelta_val_loss': [],
            'RMSprop_val_acc': [],
            'RMSprop_val_loss': [],
            'Adam_val_acc': [],
            'Adam_val_loss': []
        }

    def weight_variable(self, shape):
        initial = tf.truncated_normal(shape, stddev=0.01)
        return tf.Variable(initial)

    def bias_variable(self, shape):
        initial = tf.zeros(shape)
        return tf.Variable(initial)

    def inference(self, x, keep_prob):
        #모델 정의

        #입력층 - 은닉층
        for i, n_hidden in enumerate(self.n_hiddens):
            if i == 0:
                input = x
                input_dim = self.n_in
            else:
                input = output
                input_dim = self.n_hiddens[i-1]

            W = self.weights.append(self.weight_variable([input_dim, n_hidden]))
            b = self.biases.append(self.bias_variable([n_hidden]))

            h = tf.nn.relu(tf.matmul(input, self.weights[-1]) + self.biases[-1])
            output = tf.nn.dropout(h, keep_prob)

        #은닉층 - 출력층
        self.weights.append(self.weight_variable([self.n_hiddens[-1], self.n_out]))
        self.biases.append(self.bias_variable([self.n_out]))
        y = tf.nn.softmax(tf.matmul(output, self.weights[-1]) + self.biases[-1])
        return y

    def loss(self, y, t):
        #오차함수 정의

        cross_entropy = tf.reduce_mean(-tf.reduce_sum(
            t * tf.log(tf.clip_by_value(y, 1e-10, 1.0)),
            axis=1))

        #cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=y, labels=t)
        return cross_entropy

    def training(self, loss, lr_type):
        #학습 알고리즘 정의
        if lr_type=='SGD':
            optimizer = tf.train.GradientDescentOptimizer(0.01)
        elif lr_type == 'momentum':
            optimizer = tf.train.MomentumOptimizer(0.01, 0.9)
        elif lr_type == 'Nesterov':
            optimizer = tf.train.MomentumOptimizer(0.01, 0.9, use_nesterov=True)
        elif lr_type == 'Adagrad':
            optimizer = tf.train.AdagradOptimizer(0.1)
        elif lr_type == 'Adadelta':
            optimizer = tf.train.AdadeltaOptimizer(learning_rate=1.0, rho=0.95)
        elif lr_type == 'RMSprop':
            optimizer = tf.train.RMSPropOptimizer(0.001)
        elif lr_type == 'Adam':
            optimizer = tf.train.AdamOptimizer(learning_rate=0.001, beta1=0.9, beta2=0.999)

        train_step = optimizer.minimize(loss)
        return train_step

    def accuracy(self, y, t):
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(t, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        return accuracy

    def fit(self, X_train, Y_train, epochs=100, batch_size=100, p_keep=0.5, lr_type='SGD', verbose=1):
        #학습 처리
        x = tf.placeholder(tf.float32, shape=[None, self.n_in])
        t = tf.placeholder(tf.float32, shape=[None, self.n_out])
        keep_prob = tf.placeholder(tf.float32)

        #evaluate() 용
        self._x = x
        self._t = t
        self._keep_prob = keep_prob

        y = self.inference(x, keep_prob)
        loss = self.loss(y, t)
        train_step = self.training(loss, lr_type)
        accuracy = self.accuracy(y, t)

        init = tf.global_variables_initializer()
        sess = tf.Session()
        sess.run(init)

        #evaluate() 용
        self._y = y
        self._sess = sess

        N_train = len(X_train)
        n_batches = N_train // batch_size

        for epoch in range(epochs):
            X_, Y_ = shuffle(X_train, Y_train)

            for i in range(n_batches):
                start = i * batch_size
                end = start + batch_size

                sess.run(train_step, feed_dict={
                    x: X_[start:end],
                    t: Y_[start:end],
                    keep_prob: p_keep
                })
            val_loss = loss.eval(session=sess, feed_dict={
                x: X_validation,
                t: Y_validation,
                keep_prob: 1.0
            })
            val_accuracy = accuracy.eval(session=sess, feed_dict={
                x: X_validation,
                t: Y_validation,
                keep_prob: 1.0
            })

            #값 기록
            self._history[lr_type + '_val_loss'].append(val_loss)
            self._history[lr_type + '_val_acc'].append(val_accuracy)

            if verbose:
                print('epoch: ', epoch,
                      'validation loss: ', val_loss,
                      'validation accuracy: ', val_accuracy)
        return self._history

    def evaluate(self, X_test, Y_test):
        #평가 처리
        accuracy = self.accuracy(self._y, self._t)
        return accuracy.eval(session=self._sess, feed_dict={
            self._x: X_test,
            self._t: Y_test,
            self._keep_prob: 1.0
        })

def keras_fit(X_train, Y_train, epochs, batch_size, n_in, n_hiddens, n_out, p_keep, lr_type):
    model = Sequential()
    for i, input_dim in enumerate(([n_in] + n_hiddens)[:-1]):
        model.add(Dense(n_hiddens[i], input_dim=input_dim,
                        kernel_initializer=TruncatedNormal(stddev=0.01)))
        model.add(Activation('relu'))
        model.add(Dropout(p_keep))

    model.add(Dense(n_out, kernel_initializer=TruncatedNormal(stddev=0.01)))
    model.add(Activation('softmax'))

    if lr_type == 'SGD':
        optimizer = SGD(lr=0.01)
    elif lr_type == 'momentum':
        optimizer = SGD(lr=0.01, momentum=0.9)
    elif lr_type == 'Nesterov':
        optimizer = SGD(lr=0.01, momentum=0.9, nesterov=True)
    elif lr_type == 'Adagrad':
        optimizer = Adagrad(lr=0.01)
    elif lr_type == 'Adadelta':
        optimizer = Adadelta(rho=0.95)
    elif lr_type == 'RMSprop':
        optimizer = RMSprop(lr=0.001)
    elif lr_type == 'Adam':
        optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999)

    model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                  metrics=['accuracy'])
    hist = model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size,
                     validation_data=(X_validation, Y_validation))
    return hist

if __name__ == '__main__':
    # 1. 데이터 준비
    mnist = datasets.fetch_mldata('MNIST original', data_home='.')

    epochs = 10
    n = len(mnist.data)
    N = 30000
    N_train = 20000
    N_validation = 4000
    indices = np.random.permutation(range(n))[:N]

    X = mnist.data[indices]
    y = mnist.target[indices]
    Y = np.eye(10)[y.astype(int)]

    X_train, X_test, Y_train, Y_test = \
        train_test_split(X, Y, train_size=0.8)

    X_train, X_validation, Y_train, Y_validation = \
        train_test_split(X_train, Y_train, test_size=N_validation)

    model = DNN(n_in=784, n_hiddens=[200, 200, 200], n_out=10)

    lr_types = ['SGD', 'momentum', 'Nesterov', 'Adagrad', 'Adadelta', 'RMSprop', 'Adam']
    plot_colors = ['b','g','r','c','m','y','k']

    plt.rc('font', family='serif')
    fig = plt.figure()

    ax_acc = fig.add_subplot(111)

    for i, lr_type in enumerate(lr_types):

        #tensorflow

        #model.fit(X_train, Y_train, epochs=epochs, batch_size=200, p_keep=0.5, lr_type=lr_type)
        #accuracy = model.evaluate(X_test, Y_test)
        #print(lr_type, 'accuracy: ', accuracy)

        #ax_acc.plot(range(epochs), model._history[lr_type + '_val_acc'],
        #            label=lr_type + '_val_acc', color=plot_colors[i])

        #keras
        hist = keras_fit(X_train, Y_train, epochs=10, batch_size=200, n_in=784,
                         n_hiddens=[200, 200, 200], n_out=10, p_keep=0.5, lr_type=lr_type)
        ax_acc.plot(range(epochs), hist.history['val_acc'],
                    label=lr_type + '_val_acc', color=plot_colors[i])

    plt.xlabel('epochs')
    plt.legend(loc=4) #범례
    plt.grid(True) #격자

    '''
    plt.plot(range(epoch), model._history['val_acc'], label='acc', color='black')

    plt.xlabel('epoch')
    plt.ylabel('validation accuracy')
    '''

    plt.show()

