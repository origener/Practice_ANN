import numpy as np
import tensorflow as tf
from sklearn.utils import shuffle

M = 2 # 입력 차원
K = 3 # 클래스
n = 100 # 각 클래스 데이터 수
N = n * K # 전체 데이터 수

X1 = np.random.randn(n, M) + np.array([0, 10]) #randn 가우시안 표준분포 랜덤함수
X2 = np.random.randn(n, M) + np.array([5, 5])
X3 = np.random.randn(n, M) + np.array([10, 0])
Y1 = np.array([[1, 0, 0] for i in range(n)])
Y2 = np.array([[0, 1, 0] for i in range(n)])
Y3 = np.array([[0, 0, 1] for i in range(n)])

X = np.concatenate((X1, X2, X3), axis=0) #axis 0:행 , 1:열
Y = np.concatenate((Y1, Y2, Y3), axis=0)

W = tf.Variable(tf.zeros([M, K]))
b = tf.Variable(tf.zeros([K]))

x = tf.placeholder(tf.float32, shape=[None, M]) # 데이터갯수 None : 가변
t = tf.placeholder(tf.float32, shape=[None, K])
y = tf.nn.softmax(tf.matmul(x, W) + b) #matmul : 행렬 곱

cross_entropy = tf.reduce_mean(-tf.reduce_sum(t * tf.log(y), reduction_indices=[1]))
train_step = tf.train.GradientDescentOptimizer(0.1).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(t,1))

batch_size = 50 # 미니배치 크기
n_batchs = N // batch_size

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

for epoch in range(20):
    X_, Y_ = shuffle(X, Y)

    for i in range(n_batchs):
        start = i * batch_size
        end = start + batch_size

        sess.run(train_step, feed_dict={
            x: X_[start:end],
            t: Y_[start:end]
        })

X_, Y_ = shuffle(X, Y)

classified = correct_prediction.eval(session=sess, feed_dict={
    x: X_[0:10],
    t: Y_[0:10]
})

prob = y.eval(session=sess, feed_dict={
    x: X_[0:10]
})

print('classified: ')
print(classified)
print()
print('output probability: ')
print(prob)
