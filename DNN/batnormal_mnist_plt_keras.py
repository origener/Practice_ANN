from sklearn import datasets
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, BatchNormalization
from keras.optimizers import SGD
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from keras import backend as K

def weight_variable(shape):
    return K.truncated_normal(shape, stddev=0.01)

np.random.seed(0)

mnist = datasets.fetch_mldata('MNIST original', data_home='.')

epochs = 20
n = len(mnist.data)
N = 30000
N_train = 20000
N_validation = 4000
indices = np.random.permutation(range(n))[:N]

X = mnist.data[indices]
y = mnist.target[indices]
Y = np.eye(10)[y.astype(int)]

n_in = len(X[0])
n_hiddens = [200, 200, 200]
n_out = len(Y[0])
p_keep = 0.5
activation = 'relu'

X_train, X_test, Y_train, Y_test = \
    train_test_split(X, Y, train_size=0.8)

X_train, X_validation, Y_train, Y_validation = \
    train_test_split(X_train, Y_train, test_size=N_validation)

model = Sequential()
for i, input_dim in enumerate(([n_in] + n_hiddens)[:-1]):
    model.add(Dense(n_hiddens[i], input_dim=input_dim,
                    kernel_initializer=weight_variable))
    model.add(BatchNormalization())
    model.add(Activation(activation))
    #model.add(Dropout(p_keep))

model.add(Dense(n_out, kernel_initializer=weight_variable))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.01),
              metrics=['accuracy'])

batch_size = 200
hist = model.fit(X_train, Y_train, epochs=epochs, batch_size=200,
                 validation_data=(X_validation, Y_validation))


val_acc = hist.history['val_acc']

plt.rc('font', family='serif')
fig = plt.figure()

plt.plot(range(epochs), val_acc, label='acc', color='black')
plt.xlabel('epoch')
plt.ylabel('validation accuracy')
plt.show()

